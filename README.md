# wow_AzerothCore_db_chinese_old

#### 项目介绍
AzerothCore 3.3.5.12340 中文本地化数据库(由于AzerothCore, TrinityCore本地化表结构变化,本库只能用于参考)

#### 软件架构
MYsql数据库

https://github.com/azerothcore/azerothcore-wotlk

#### 使用说明

使用mysql数据库管理软件导入world数据库

#### 参与贡献
希望大家能一起完善数据本地化
